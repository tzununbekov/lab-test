// entrypoint.js
// attribution: https://github.com/elliotforbes/go-webassembly-framework

const go = new Go();
WebAssembly.instantiateStreaming(fetch("lib.wasm"), go.importObject).then(
  result => {
    go.run(result.instance);
  }
);
